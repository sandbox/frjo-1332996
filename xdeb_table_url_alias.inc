<?php

/**
 * @file
 * Make a copy of the role table. To use this you must create a table named
 * foobar_copy with the same structure as foobar.
 */

/**
 * There are four essential components to set up in your constructor:
 *  $this->source - An instance of a class derived from MigrateSource, this
 *    will feed data to the migration.
 *  $this->destination - An instance of a class derived from MigrateDestination,
 *    this will receive data that originated from the source and has been mapped
 *    by the Migration class, and create Drupal objects.
 *  $this->map - An instance of a class derived from MigrateMap, this will keep
 *    track of which source items have been imported and what destination objects
 *    they map to.
 *  Mappings - Use $this->addFieldMapping to tell the Migration class what source
 *    fields correspond to what destination fields, and additional information
 *    associated with the mappings.
 */
class xdebTableUrlAliasMigration extends Migration {
  public function __construct() {
    parent::__construct();
    // Human-friendly description of your migration process. Be as detailed as you like.
    $this->description = 'Migrate the xdeb url_alias table by copying it.';

    $this->dependencies = array('xdebNodePage', 'xdebNodeStory', 'xdebNodeBlog');

    $table_name = 'url_alias';

    // Create a map object for tracking the relationships between source rows
    // and their resulting Drupal objects. Usually, you'll use the MigrateSQLMap
    // class, which uses database tables for tracking. Pass the machine name
    // (BeerTerm) of this migration to use in generating map and message tables.
    // And, pass schema definitions for the primary keys of the source and
    // destination - we need to be explicit for our source, but the destination
    // class knows its schema already.
    $this->map = new MigrateSQLMap($this->machineName,
      array(
        'pid' => array(
          'type' => 'int',
          'unsigned' => TRUE,
          'not null' => TRUE,
          'alias' => 'ua',
        )
      ),
        MigrateDestinationTable::getKeySchema($table_name)
    );

    // We are getting data from tables in the Drupal default database - first,
    // set up a query for this data.
    $query = db_select(XDEB_MIGRATION_DATABASE_NAME . '.url_alias', 'ua')
      ->fields('ua')
      ->condition('ua.src', 'taxonomy/%', 'LIKE');
    $query->orderBy('ua.pid', 'ASC');

    // Create a MigrateSource object, which manages retrieving the input data.
    $this->source = new MigrateSourceSQL($query);

    // Set up our destination - Tabel in this case.
    $this->destination = new MigrateDestinationTable($table_name);

    // Assign mappings TO destination fields FROM source fields.
    $this->addFieldMapping('source', 'src');
    $this->addFieldMapping('alias', 'dst');
    $this->addFieldMapping('language')->defaultValue(LANGUAGE_NONE);
  }

  public function prepareRow($current_row) {
    preg_match('%^taxonomy/term/([0-9]+)%', $current_row->src, $matches);
    $tid_old = $matches[1];

    $querytermblog = db_select('migrate_map_xdebtermblog', 'mtb')
      ->fields('mtb', array('destid1'))
      ->condition('mtb.sourceid1', $tid_old, '=');

    $querytermstory = db_select('migrate_map_xdebtermstory', 'mtb')
      ->fields('mtb', array('destid1'))
      ->condition('mtb.sourceid1', $tid_old, '=');

    $querytermtags = db_select('migrate_map_xdebtermtags', 'mtb')
      ->fields('mtb', array('destid1'))
      ->condition('mtb.sourceid1', $tid_old, '=');

    if ($result = $querytermblog->execute()->fetchObject()) {
      $tid_new = $result->destid1;
    }
    else if ($result = $querytermstory->execute()->fetchObject()) {
      $tid_new = $result->destid1;
    }
    else if ($result = $querytermtags->execute()->fetchObject()) {
      $tid_new = $result->destid1;
    }

    if (!empty($tid_new)) {
      $current_row->src = preg_replace('%[0-9]+%', "$tid_new", $current_row->src, 1);
    }

    // Replace depth "all" with depth 1. Needed in Drupal 7.
    $current_row->src = str_replace('/all', '/1', $current_row->src);

    // We could also have used this function to decide to skip a row, in cases
    // where that couldn't easily be done through the original query. Simply
    // return FALSE in such cases.
    return TRUE;
  }
}
