<?php

/**
 * @file
 * Make a copy of the role table. To use this you must create a table named
 * foobar_copy with the same structure as foobar.
 */

/**
 * There are four essential components to set up in your constructor:
 *  $this->source - An instance of a class derived from MigrateSource, this
 *    will feed data to the migration.
 *  $this->destination - An instance of a class derived from MigrateDestination,
 *    this will receive data that originated from the source and has been mapped
 *    by the Migration class, and create Drupal objects.
 *  $this->map - An instance of a class derived from MigrateMap, this will keep
 *    track of which source items have been imported and what destination objects
 *    they map to.
 *  Mappings - Use $this->addFieldMapping to tell the Migration class what source
 *    fields correspond to what destination fields, and additional information
 *    associated with the mappings.
 */
class xdebTableNodeCounterMigration extends Migration {
  public function __construct() {
    parent::__construct();
    // Human-friendly description of your migration process. Be as detailed as you like.
    $this->description = 'Migrate the xdeb node_counter table by copying it.';

    $this->dependencies = array('xdebNodePage', 'xdebNodeStory', 'xdebNodeBlog');

    $destination_key = array(
      'nid' => array(
        'type' => 'int',
        'unsigned' => FALSE,
        'not null' => TRUE,
      ),
    );

    // Create a map object for tracking the relationships between source rows
    // and their resulting Drupal objects. Usually, you'll use the MigrateSQLMap
    // class, which uses database tables for tracking. Pass the machine name
    // (BeerTerm) of this migration to use in generating map and message tables.
    // And, pass schema definitions for the primary keys of the source and
    // destination - we need to be explicit for our source, but the destination
    // class knows its schema already.
    $this->map = new MigrateSQLMap($this->machineName,
      array(
        'nid' => array(
          'type' => 'int',
          'unsigned' => FALSE,
          'not null' => TRUE,
          'alias' => 'nc',
        )
      ),
      $destination_key
    );

    // We are getting data from tables in the Drupal default database - first,
    // set up a query for this data.
    $query = db_select(XDEB_MIGRATION_DATABASE_NAME . '.node_counter', 'nc')
      ->fields('nc');
    $query->join('node', 'n', 'nc.nid = n.nid');
    $query->orderBy('nc.nid', 'ASC');

    // Create a MigrateSource object, which manages retrieving the input data.
    $this->source = new MigrateSourceSQL($query);

    // Set up our destination - TabelCopy in this case.
    $this->destination = new MigrateDestinationTableCopy('node_counter', $destination_key);
  }
}
